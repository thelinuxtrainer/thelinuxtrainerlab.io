---
title: Linux Professional Institute LPIC-3 Mixed Environments
nav_order: 401
parent: LPI Exam Learning Resources
grand_parent: Home
---


# Linux Professional Institute LPIC-3 Mixed Environments

The Linux Professional Institute's multi-level professional certification program culminates in the LPIC-3 certification (LPI). LPIC-3 is intended for enterprise-level Linux professionals and is the industry's highest level of professional, distribution-neutral Linux certification. There are four distinct LPIC-3 speciality certifications available. Passing any of the four exams results in LPIC-3 certification for that specialty.

The LPIC-3 Mixed Environments certification covers the administration of Linux systems in mixed environments across a business.


[Overview](https://www.lpi.org/our-certifications/lpic-3-300-overview){: .btn .btn-purple }  [Exam Objectives](https://wiki.lpi.org/wiki/LPIC-300_Objectives_V3.0){: .btn .btn-purple }
