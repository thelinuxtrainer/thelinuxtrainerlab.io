---
title: Linux Professional Institute LPIC-3 Security
nav_order: 501
parent: LPI Exam Learning Resources
grand_parent: Home
---

# Linux Professional Institute LPIC-3 Security

The Linux Professional Institute's multi-level professional certification program culminates in the LPIC-3 certification (LPI). LPIC-3 is intended for enterprise-level Linux professionals and is the industry's highest level of professional, distribution-neutral Linux certification. There are four distinct LPIC-3 speciality certifications available. Passing any of the four tests results in LPIC-3 certification for that specialty.

The LPIC-3 Security certification covers enterprise-wide Linux system administration with a focus on security.

[Overview](https://www.lpi.org/our-certifications/lpic-3-303-overview){: .btn .btn-purple }  [Exam Objectives](https://wiki.lpi.org/wiki/LPIC-303_Objectives_V3.0){: .btn .btn-purple }

