---
title: Devops Tools Engineer Exam Learning Resources
nav_order: 802
parent: Linux Professional Institute DevOps Tools Engineer
grand_parent: LPI Exam Learning Resources
has_children: true
---

# Devops Tools Engineer Exam Learning Resources 

The links for studying [Exam 701: DevOps Tools Engineer](https://www.lpi.org/our-certifications/exam-701-objectives) offered here are found from the LPI blog. When you've done studying a topic, check the checkbox next to it. This website was inspired by [Hamidreza Josheghani's](https://github.com/pyguy) great work, which you caan find at [LPI DevOps study resources](https://github.com/pyguy/lpi-devops-study). This website, which I have designed for you, has up-to-date information as of February 2022, as well as training modules and labs that I have designed for exam practice and real-world skill development.

[I](https://www.linkedin.com/in/chinthakadeshapriya/) wish you  Good luck on your [LPI DevOps Tools Engineer Exam](https://www.lpi.org/our-certifications/exam-701-objectives).


