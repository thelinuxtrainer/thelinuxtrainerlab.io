---
title: Linux Professional Institute LPIC-1 
nav_order: 201
parent: LPI Exam Learning Resources
grand_parent: Home
---

# Linux Professional Institute LPIC-1

**The world’s largest and most recognized Linux certification**, LPIC-1 is the first certification in the Linux Professional Institute's multi-level Linux professional certification program (LPI). The LPIC-1 will assess the candidate's competence to do command-line maintenance activities, install and configure a machine running Linux, and configure basic networking.

The LPIC-1 is intended to reflect current research and evaluate a candidate's knowledge of real-world system management. The objectives are linked to real-world job abilities, which we determine during exam formulation through job task analysis surveying.

[Overview](https://www.lpi.org/our-certifications/lpic-1-overview){: .btn .btn-purple }  [Exam Objectives](https://wiki.lpi.org/wiki/LPIC-1_Objectives_V5.0){: .btn .btn-purple }

