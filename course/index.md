---
title: LPI Exam Learning Resources
nav_order: 2
parent: Home
has_children: true
---

# The Linux Professional Institute

The Linux Professional Institute (LPI) is a global certification organization and career development resource for open source professionals. It is the world's first and largest vendor-neutral Linux and open source certification body, with over 200,000 certification holders. LPI certifies professionals in more than 180 countries, offers exams in multiple languages, and works with hundreds of training partners.
