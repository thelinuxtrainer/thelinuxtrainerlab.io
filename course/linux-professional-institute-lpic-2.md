---
title: Linux Professional Institute LPIC-2 
nav_order: 301
parent: LPI Exam Learning Resources
grand_parent: Home
---

# Linux Professional Institute LPIC-2

LPIC-2 is the second certification in the Linux Professional Institute's multi-level professional certification program (LPI). The LPIC-2 will assess a candidate's competence to manage small to medium–sized mixed networks.

[Overview](https://www.lpi.org/our-certifications/lpic-2-overview){: .btn .btn-purple }  [Exam Objectives](https://wiki.lpi.org/wiki/LPIC-2_Objectives_V4.5){: .btn .btn-purple }
