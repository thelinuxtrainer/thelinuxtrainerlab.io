---
title: Linux Professional Institute Linux Essentials 
nav_order: 101
parent: LPI Exam Learning Resources
grand_parent: Home
---

# Linux Professional Institute Linux Essentials

**Demonstrate to employers that you have the fundamental abilities needed for your next job or promotion**.

Linux use is increasing globally as individuals, governments, and sectors ranging from automotive to space exploration embrace open source technologies. Because of the growth of open source in the industry, traditional Information and Communication Technology (ICT) job roles are being redefined to demand more Linux capabilities. Whether you're just starting out in open source or looking to improve your career, independently confirming your skill set can help you stand out to hiring managers or your management team.

The Linux Essentials certificate is also an excellent starting point for the more comprehensive and sophisticated Linux Professional certification route.

[Owerview](https://www.lpi.org/our-certifications/linux-essentials-overview){: .btn .btn-purple } [Exam Objectives](https://wiki.lpi.org/wiki/Linux_Essentials_Objectives_V1.6){: .btn .btn-purple } 
