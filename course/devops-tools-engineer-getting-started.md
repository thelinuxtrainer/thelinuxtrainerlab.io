---
title: Getting Started
nav_order: 803
parent: Devops Tools Engineer Exam Learning Resources
grand_parent: Linux Professional Institute DevOps Tools Engineer
---

#  1. Getting Started 

- [ ] [Getting Started](https://www.lpi.org/blog/2018/01/09/devops-tools-introduction-01-getting-getting-started-started)
- [ ] [DevOps Tools Engineer Objectives V1 - LPI Wiki](https://wiki.lpi.org/wiki/DevOps_Tools_Engineer_Objectives_V1)

