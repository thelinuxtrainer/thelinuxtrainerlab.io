---
title: Home
nav_order: 1
permalink: /
has_children: true
---

# Free Is The Freedom

Education should never be a financial barrier, particularly for Linux and FOSS. This website's major objective is to provide you with a completely free collection of study tools for all Linux Professional Institute certification tests. I intend to share with you my [over 20 years of industry expertise](https://www.linkedin.com/in/chinthakadeshapriya/) as a Linux System Engineer, DevOps consultant, Red Hat Certified Instructor, and Red Hat Certified Architect certificate that I acquired ten years ago.

I like how the LPI approaches Linux Distribution Neutral in training and certification. This website's mission is not to bind you to a single exam objective, but rather to equip you with enough hands-on practical experience through our industry-oriented lab practices that are incorporated with exam study tools.
